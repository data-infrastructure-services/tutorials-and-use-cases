#FROM mambaorg/micromamba:0.15.3

#USER root
#COPY environment.yml /tmp/env.yaml

#RUN apt-get update && apt-get install build-essential -y
#RUN apt-get install git -y

#RUN micromamba install -y -n base -f /tmp/env.yaml && \
#    micromamba clean --all --yes

#ENV PATH=$PATH:/micromamba/pcondabin:/micromamba/bin

#RUN python -m ipykernel install --name python3 --display-name "python3" --user 

#ENV NB_USER micromamba

#singularity
FROM ubuntu:xenial
#
RUN apt-get update && apt-get install -y wget bzip2 build-essential git
RUN wget -qO-  https://micromamba.snakepit.net/api/micromamba/linux-64/latest | tar -xvj bin/micromamba  \
    && ./bin/micromamba shell init -s bash -p /opt/conda

COPY environment.yml /tmp/env.yaml

RUN rm -r /envs || echo "Got no envs"
RUN ./bin/micromamba config set channel_priority flexible || echo "Could not set priority"
RUN ./bin/micromamba create -y -f /tmp/env.yaml -p /envs && \
    ./bin/micromamba clean --all --yes

RUN apt-get clean autoremove --yes
ENV PATH=$PATH:/micromamba/pcondabin:/micromamba/bin:/envs/bin

#:/root/micromamba/bin
RUN python -m ipykernel install --name python3 --display-name "python3" --user

USER root
ENV NB_USER micromamba

