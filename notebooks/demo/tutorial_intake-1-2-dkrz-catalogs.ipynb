{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Intake I part 2 - DKRZ catalog scheme, strategy and services"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "```{admonition} Overview\n",
    ":class: dropdown\n",
    "\n",
    "![Level](https://img.shields.io/badge/Level-Introductory-green.svg)\n",
    "\n",
    "\n",
    "🎯 **objectives**: Learn what `intake-esm` ESM-collections DKRZ offer\n",
    "\n",
    "⌛ **time_estimation**: \"15min\"\n",
    "\n",
    "☑️ **requirements**: None\n",
    "\n",
    "© **contributors**: k204210\n",
    "\n",
    "⚖ **license**:\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Agenda\n",
    ":class: tip\n",
    "\n",
    "In this part, you learn\n",
    "\n",
    "1. [DKRZ intake-esm catalog schema](#examples)\n",
    "1. [DKRZ intake-esm catalogs for project data](#examples)\n",
    "1. [Catalog dependencies on different stores](#stores)\n",
    "1. [Workflow at Levante for collecting and merging catalogs into main catalog](#workflow)\n",
    "    \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"examples\"></a>\n",
    "\n",
    "## DKRZ intake-esm catalog strategy and schema\n",
    "\n",
    "DKRZ catalogs aim at using one common scheme for its attributes so that **combining** catalogs and working with multiple catalogs on the same time will be easy. In collaboration with NextGEMS scientists, we agreed on some attribute names that DKRZ intake-esm catalogs should be equipped with. The resulting scheme is named *cataloonies scheme*.\n",
    "\n",
    "```{note}\n",
    "\n",
    "The cataloonies scheme is not a standard for anything but it is evolving and will be adapted to use cases. It is mainly influenced by ICON output and the CMIP standard. If you have suggestions, please contact us.\n",
    "\n",
    "```\n",
    "\n",
    "- As a result, you will find **redundant** attributes in project catalogs which have the same meaning, e.g:\n",
    "    - source_id, model_id, model\n",
    "    - member_id, ensemble_member, simulation_id\n",
    "- Which of these attributes are loaded into the python workflow can be set (see intake-1).\n",
    "- You will find only **one version** for each *atomic dataset* in each catalog. This is the most recent one available in the store. An atomic dataset is found if unique values are set for all catalog attributes with one exception: it covers the entire time span of the simulation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"examples\"></a>\n",
    "\n",
    "### The cataloonies scheme\n",
    "\n",
    "ESM script developers, project scientists and data managers together defined some attribute names that DKRZ intake-esm catalogs should be equipped with. One benefit, originated by the composition of this working group, is that these attribute names can be used throughout the research data life cycle: At the earliest point, for model raw output, but also at the latest point, for data that is standardized and published.\n",
    "\n",
    "The integration of intake-esm catalog generation into ESM run scripts is planned. That will enable the usage of intake and with it the easy usage and configuration of the python software stack for data processing from the beginning of data life.\n",
    "\n",
    "Already existing catalogs will be provided with the newly defined attributes. For some, the values will fall back to *None* as there is no easy way to retrieve the values without looking into the asset which is technically not implementable when taking into account the amount of published project data. For CMIP6-like projects, we can take missing information from the *cmor-mip-tables* which represent the data standard of the project."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hide-input"
    ]
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "cataloonies_raw=[[\"#\",\"Attribute name and column name\",\"Examples\",\"Description\",\"Comments\"],\n",
    "[1,\"project\",\"DYAMON-WINTER\",\"The project in which the data was produced\",],\n",
    "[2,\"institution_id\",\"MPIM-DWD-DKRZ\",\"The institution that runs the simulations\",],\n",
    "[3,\"source_id\",\"ICON-SAP-5km\",\"The Earth System Model which produced the simulations\",],\n",
    "[4,\"experiment_id\",\"DW-CPL / DW-ATM\",\"The short term for the experiment that was run\",],\n",
    "[5,\"simulation_id\",\"dpp1234\",\"The simulation/member/realization of the ensemble.\",],\n",
    "[6,\"realm\",\"atm / oce\",\"The submodel of the ESM which produces the output.\",],\n",
    "[7,\"frequency\",\"PT1h or 1hr – Style\",\"The frequency of the output\",\"ICON uses ISO format\"],\n",
    "[8,\"time_reduction\",\"mean / inst / timmax /…\",\"The method used for sampling and averaging along time. The same as the time part of cell_methods.\",],\n",
    "[9,\"grid_label\",\"gn\",\"A clear description for the grid for distingusihing between native and regridded grids.\",],\n",
    "[10,\"grid_id\",\"\",\"A specific identifier of the grid.\",\"we might need more than one (e.g. horizontal + vertical)\"],\n",
    "[11,\"variable_id\",\"tas\",\"The CMIP short term of the variable.\",],\n",
    "[12,\"level_type\",\"pressure_level, atmosphere_level\",\"The vertical axis type used for the variable.\",],\n",
    "[13,\"time_min\",1800,\"The minimal time value covered by the asset.\",],\n",
    "[14,\"time_max\",1900,\"The maximal time value covered by the asset.\",],\n",
    "[15,\"format\",\"netcdf/zarr/…\",\"The format of the asset.\",],\n",
    "[16,\"uri\",\"url,path-to-file\",\"The uri used to open and load the asset.\",],\n",
    "[17,\"(time_range)\",\"start-end\",\"Combination of time_min and time_max.\",]]\n",
    "pd.DataFrame(cataloonies_raw[1:],columns=cataloonies_raw[0])[cataloonies_raw[0][1:-1]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"examples\"></a>\n",
    "\n",
    "## DKRZ intake-esm catalogs for community project data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Jobs we do for you\n",
    "\n",
    "- We **make all catalogs available** \n",
    "    - under `/pool/data/Catalogs/` for logged-in HPC users\n",
    "    - in the [cloud](https://gitlab.dkrz.de/data-infrastructure-services/intake-esm/-/tree/master/esm-collections)\n",
    "- We **create and update** the content of project's catalogs regularly by running scripts which are automatically executed and called _cronjobs_. We set the creation frequency so that the data of the project is updated sufficently quickly.\n",
    "    - The updated catalog __replaces__ the outdated one. \n",
    "    - The updated catalog is __uploaded__ to the DKRZ swift cloud \n",
    "    - We plan to provide a catalog that tracks data which is __removed__ by the update."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The data bases of project catalogs\n",
    "\n",
    "**Creation of the `.csv.gz` table :**\n",
    "\n",
    "1. A file list is created based on a `find` shell command on the project directory in the data pool.\n",
    "1. For the column values, filenames and pathes are parsed according to the project's `path_template` and `filename_template`. These templates need to be constructed with attribute values requested and required by the project.\n",
    "    - Filenames that cannot be parsed are sorted out\n",
    "1. If more than one version is found for a dataset, only the most recent one is kept.\n",
    "1. Depending on the project, additional columns can be created by adding project's specifications.\n",
    "    - E.g., for CMIP6, we added a `OpenDAP` column which allows users to access data from everywhere via `http`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"examples\"></a>\n",
    "\n",
    "By 2022, we offer you project data for\n",
    "\n",
    "```{tabbed} CMIP6-like projects \n",
    "\n",
    "- 💾  **projects** :\n",
    "    - [CMIP6](https://c6de.dkrz.de/)\n",
    "    - [PalMod2](https://www.palmod.de/)\n",
    "- **Data location**:\n",
    "    - is hosted within the [cmip-data-pool](cmip-data-pool.dkrz.de) accessible for all dkrz users under `/pool/data/PROJECT`\n",
    "- **Attributes**:\n",
    "    - the catalog's attributes are explained [here](https://goo.gl/v1drZl)\n",
    "    - `source_id` and `experiment_id` are the most important attributes. A unique `source_id` refers to one and only the one same model. Different institutions can use it but it is the same model. An `experiment_id` can be found only in *one activity*.\n",
    "    - values of `member_id` are rather arbitrary. Some member might never be published, others have been retracted. There is no gurantee in having *r1i1p1f1* availabe.\n",
    "- **Variable definition**:\n",
    "    - a **unique variable** is a combination of `table_id` and `variable_id`. A variable can look different from one table to another. I.e., a variable can have different dimensions for monthly frequency than it has for daily frequency.\n",
    "    \n",
    "```\n",
    "\n",
    "```{tabbed} CMIP5-like projects\n",
    "\n",
    "- 💾  **projects** :\n",
    "    - CMIP5: \n",
    "    - [CORDEX](https://is-enes-data.github.io/cordex_archive_specifications.pdf)\n",
    "- **Data location**:\n",
    "    - CMIP5:\n",
    "        - Only a small subset is still available on the pool's common and shared disk resource due to a lack of disk storage capacity. But most of the data have been archived and can be accessed via `jblob`.\n",
    "    - CORDEX:\n",
    "        - On disk, CORDEX data are disseminated across different storage projects\n",
    "- **Attributes**:\n",
    "    - In comparison to CMIP6-like projects, such projects build on the older CMIP5 standard. Therefore, some attributes have different names.\n",
    "    - Regional model data includes additional attributes incomparison to CMIP5: `CORDEX_domain`, `driving_model_id` and `rcm_version_id`.\n",
    "- **Variable definition**:\n",
    "    - A **unique variable** is a combination of `mip_table` and `variable`. A variable can look different from one table to another. I.e., a variable can have different dimensions for monthly frequency than it has for daily frequency.\n",
    "```\n",
    "\n",
    "```{tabbed} ESM-raw-output-near projects\n",
    "\n",
    "- 💾  **projects** :\n",
    "    - [DYAMOND](https://easy.gems.dkrz.de/DYAMOND/index.html)\n",
    "    - [NextGEMS](https://easy.gems.dkrz.de/DYAMOND/NextGEMS/index.html)\n",
    "    - [ERA5](https://docs.dkrz.de/doc/dataservices/finding_and_accessing_data/era_data/index.html):\n",
    "- **Data location**:\n",
    "    - For disk projects, mostly linked under `/pool/data`\n",
    "- **Attributes**:\n",
    "    - We try to use the cataloonies schema for catalogs of all other projects. For reanalysis products, it cannot be entirely fulfilled as the data is available in GRIB format.\n",
    "\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"examples\"></a>\n",
    "\n",
    "## Catalog dependencies on different stores\n",
    "\n",
    "DKRZ's catalog naming convention distinguishes between the different storage formats for as long as data access to stores like archive is either not possible or very different to disk access. The specialties of the storages are explained in the following."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "```{tabbed} Disk\n",
    "\n",
    "- **Way of data access**:\n",
    "    - `uri` contain *pathes* on levante's lustre filesystem\n",
    "- **User requirements for data access**:\n",
    "    - users muste be **logged in** to levante to *access* the data (exception: opendap_url column, see [introduction]())\n",
    "- **Provider requirements**:\n",
    "    - pathes of a valid catalog must be *readable for everyone*\n",
    "\n",
    "```\n",
    "\n",
    "```{tabbed} Cloud\n",
    "\n",
    "- **Way of data access**:\n",
    "    - `uri` contain *links* to datasets in dkrz's swift cloud storage which can be opened with `xarray` and therefore `intake`\n",
    "    - If the asset's `format` is *zarr* (specified in the `format` column), use `zarr_kwargs` in the `to_dataset_dict()` function\n",
    "- **User requirements for data access**:\n",
    "    - None. Users can access it from everywhere if internet connection is sufficient\n",
    "- **Provider requirements**:\n",
    "    - links in a valid catalog must point at datasets in **open containers**\n",
    "\n",
    "```\n",
    "\n",
    "```{tabbed} Archive\n",
    "\n",
    "- **Way of data access**:\n",
    "    - `uri` is empty i.e. no direct data access via intake is possible. If the catalog contains a `jblob_file` column, users can however download the data via *jblob* on levante (see next point).\n",
    "- **User requirements for data access**:\n",
    "    - users muste be **logged in** to levante to *access* the data. After loading the module on levante via `module load jblob`,  e.g. for CMIP5, an example command is `jblob --cmip5-file DSET` where dset is a value of `jblob_file`, e.g. `cmip5.output1.BCC.bcc-csm1-1.abrupt4xCO2.fx.atmos.fx.r0i0p0.v1.areacella.areacella_fx_bcc-csm1-1_abrupt4xCO2_r0i0p0.nc`\n",
    "- **Provider requirements**:\n",
    "    - None\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"examples\"></a>\n",
    "\n",
    "## Preparing project catalogs for DKRZ's main catalog\n",
    "\n",
    "1. Use attributes of existing catalogs and/or templates in `/pool/data/Catalogs/Templates` but at least `uri`, `format` and `project`.\n",
    "1. Set permissions to *readable for everyone* for\n",
    "    - the data referenced in the catalog under `uri`\n",
    "    - the catalog itself\n",
    "1. Use the naming convention for dkrz catalogs ( `dkrz_PROJECT_STORE` ) for your catalog\n",
    "1. Link the catalog via `ln -s PATH-TO-YOUR-CATALOG /pool/data/Catalogs/Candidates/YOUR-CATALOG`\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your catalog then will be catched by a cronjob which\n",
    "\n",
    "1. tests your catalog\n",
    "    - against the catalog naming convention\n",
    "    - open, search and load\n",
    "    - if for disk, are all `uri` values *readable*?\n",
    "1. merges or creates your catalog\n",
    "    - if a catalog for the specified project exists in `/pool/data/Catalogs/`, they will be merged if possible. Entries of your catalog will be merged if they are no duplicates.\n",
    "    - else, your catalog will be written to `/work/ik1017/Catalogs` and a link will be set in `/pool/data/Catalogs/`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{seealso}\n",
    "This tutorial is part of a series on `intake`:\n",
    "* [Part 1: Introduction](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-1-introduction.html)\n",
    "* [Part 2: Modifying and subsetting catalogs](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-2-subset-catalogs.html)\n",
    "* [Part 3: Merging catalogs](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-3-merge-catalogs.html)\n",
    "* [Part 4: Use preprocessing and create derived variables](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-4-preprocessing-derived-variables.html)\n",
    "* [Part 5: How to create an intake catalog](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-5-create-esm-collection.html)\n",
    "\n",
    "- You can also do another [CMIP6 tutorial](https://intake-esm.readthedocs.io/en/latest/user-guide/cmip6-tutorial.html) from the official intake page.\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "taucenv",
   "language": "python",
   "name": "taucenv"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
