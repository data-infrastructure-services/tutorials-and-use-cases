{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Advanced *Interactive time series plot* of a global yearly mean anomaly with **xarray**, **pandas** and **hvplot** using CMIP6 data\n",
    "\n",
    "We will show how to combine, analyse and quickly plot data of the Coupled Model Intercomparison Project [CMIP6](https://pcmdi.llnl.gov/CMIP6/). We will choose one variable of multiple experiments and compare the results of different models. In particular, we analyse the historical experiment in combination with one of the shared socioeconomic pathway (ssp) experiments. \n",
    "\n",
    "This Jupyter notebook is meant to run in the [Jupyterhub](https://jupyterhub.dkrz.de/hub/login?next=%2Fhub%2Fhome) server of the German Climate Computing Center [DKRZ](https://www.dkrz.de/). The DKRZ hosts the CMIP data pool including 4 petabytes of CMIP6 data. Please, choose the Python 3 unstable kernel on the Kernel tab above, it contains all the common geoscience packages. See more information on how to run Jupyter notebooks at DKRZ [here](https://www.dkrz.de/up/systems/mistral/programming/jupyter-notebook).\n",
    "\n",
    "Running this Jupyter notebook in your premise, which is also known as [client-side](https://en.wikipedia.org/wiki/Client-side) computing, will require that you install the necessary packages and download data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Learning Objectives\n",
    "\n",
    "- How to access a dataset from the DKRZ CMIP data pool with `intake-esm`\n",
    "- How to calculate global field means and yearly means with `xarray` and `numpy`\n",
    "- How to visualize the results with `hvplot`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import intake\n",
    "import pandas as pd\n",
    "import hvplot.pandas\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we need to set the `variable_id` which we like to plot. This is a selection of the most often analysed variables:\n",
    "\n",
    "- `tas` is *Near-surface Air Temperature*\n",
    "- `pr` is *Precipitation*\n",
    "- `psl` is *Sea level pressure*\n",
    "- `tasmax` is *Near-surface Maximum Air Temperature*\n",
    "- `tasmin` is *Near-surface Minimum Air Temperature*\n",
    "- `clt` is *Total Cloud Cover Percentage*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Choose the variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Choose one of\n",
    "# pr, psl, tas, tasmax, tasmin, clt\n",
    "variable_id = \"tas\"\n",
    "%store -r"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get formating done automatically according to style `black`\n",
    "#%load_ext lab_black"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `intake-esm` software reads *Catalogs* which we use to **find, access and load** the data we are interested in. Daily updated CMIP6 catalogs are provided in DKRZ's cloud [swift](https://swiftbrowser.dkrz.de/public/dkrz_a44962e3ba914c309a7421573a6949a6/intake-esm/).\n",
    "\n",
    "Similar to the shopping catalog at your favorite online bookstore, the intake catalog contains information (e.g. model, variables, and time range) about each dataset that you can access before loading the data. It means that thanks to the catalog, you can find out where the \"book\" is just by using some keywords and you do not need to hold it in your hand to know the number of pages.\n",
    "\n",
    "We specify the catalog descriptor for the intake package. The catalog descriptor is created by the DKRZ developers that manage the catalog, you do not need to care so much about it, knowing where it is and loading it is enough:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Path to master catalog on the DKRZ server\n",
    "#dkrz_catalog=intake.open_catalog([\"https://dkrz.de/s/intake\"])\n",
    "#\n",
    "#only for the web page we need to take the original link:\n",
    "parent_col=intake.open_catalog([\"https://gitlab.dkrz.de/data-infrastructure-services/intake-esm/-/raw/master/esm-collections/cloud-access/dkrz_catalog.yaml\"])\n",
    "list(parent_col)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "col=parent_col[\"dkrz_cmip6_disk\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Browsing through the catalog"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define a query and specify *keyvalues* for search facets in order to search the catalogs. Possible **Search facets** are all columns of the table identified by its name.\n",
    "\n",
    "In this example, we compare the MPI-ESM1-2-HR model of the Max-Planck-Institute and the AWI-CM-1-1-MR from the Alfred Wegner Institute for 3 different experiments. CMIP6 comprises many experiments with lots of simulation members and we will use some of them. You can find more information in the [CMIP6 Model and Experiment Documentation](https://pcmdi.llnl.gov/CMIP6/Guide/dataUsers.html#5-model-and-experiment-documentation).\n",
    "\n",
    "We will concatenate historical experiment with two different Shared Socioeconomic Pathway (SSPs) scenarios. The historical experiment uses best estimates for anthropogenic and natural forcing for simulating the historical period 1850-2014. SSPs are scenarios of projected socioeconomic global changes.  \n",
    "- *historical*\n",
    "\n",
    "    This experiments usese the best estimates for anthropogenic and natural forcing for simulating the historical period 1850-2014. \n",
    "- *ssp245*\n",
    "\n",
    "    The 45 corresponds to the growth in radiative forcing reached by 2100, in this case, 4.5 W/m2 or ~650 ppm CO2 equivalent\n",
    "- *ssp585*\n",
    "\n",
    "    The 85 corresponds to the growth in radiative forcing reached by 2100, in this case, 8.5 W/m2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query = dict(\n",
    "    variable_id=variable_id,\n",
    "    table_id=\"Amon\",\n",
    "    experiment_id=[\"historical\", \"ssp585\"], # we have excluded \"ssp245\" from the list because it would take 15min to finish the nb\n",
    "    source_id=[\"MPI-ESM1-2-HR\", \"AWI-CM-1-1-MR\"],\n",
    ")\n",
    "col.df[\"uri\"]=col.df[\"uri\"].str.replace(\"lustre/\",\"lustre02/\") \n",
    "cat = col.search(**query)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a look into the new catalog subset `cat`. We use the underlaying `pandas` dataframe object `df` to display the catalog as a table. Each row refers to one file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat.df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Loading the data\n",
    "\n",
    "We can load the data into memory with only one code line. The catalog's `to_dataset_dict` command will aggregate and combine the data from files into comprehending `xarray` datasets using the specifications from the intake descriptor file. The result is a `dict`-type object where keys are the highest granularity which cannot be combined or aggregated anymore and values are the datasets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xr_dset_dict = cat.to_dataset_dict(cdf_kwargs={\"chunks\":{\"time\":1}})\n",
    "print(xr_dset_dict.keys())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xr_dset_dict['ScenarioMIP.DWD.MPI-ESM1-2-HR.ssp585.Amon.gn']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Global yearly mean calculation\n",
    "\n",
    "We define a function for calculation the global mean by weighting grid boxes according to their surface area. Afterwards, we groupby years and calculate the yearly mean. This all is done by using `xarray`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def global_yearly_mean(hist_dsets):\n",
    "    # Get weights\n",
    "    weights = np.cos(np.deg2rad(hist_dsets.lat))\n",
    "    # Tas weighted\n",
    "    variable_array = hist_dsets.get(variable_id)\n",
    "    variable_weights = variable_array.weighted(weights)\n",
    "    # Tas global mean:\n",
    "    variable_globalmean = variable_weights.mean((\"lon\", \"lat\"))\n",
    "    # Tas yearly mean:\n",
    "    variable_gmym = variable_globalmean.groupby(\"time.year\").mean(\"time\")\n",
    "    return variable_gmym.values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Historical reference period\n",
    "\n",
    "We define the period from `1851-1880` as our reference period. In the following, we calculate future simulation anomalies from that period. But first things first, we need the global yearly mean for that period:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "historical = [key for key in xr_dset_dict.keys() if \"historical\" in key][0]\n",
    "dshist = xr_dset_dict[historical]\n",
    "dshist_ref = dshist.sel(time=dshist.time.dt.year.isin(range(1851, 1881)))\n",
    "# 10member\n",
    "var_ref = global_yearly_mean(dshist_ref)\n",
    "var_refmean = var_ref.mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get Meta Data\n",
    "\n",
    "In order to label the plot correctly, we retrieve the attributes `long_name` and `units`from the chosen variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lname = dshist.get(variable_id).attrs[\"long_name\"]\n",
    "units = dshist.get(variable_id).attrs[\"units\"]\n",
    "label = \"Delta \" + lname + \"[\" + units + \"]\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Calculate Anomaly\n",
    "\n",
    "1. We save the result - the anomaly values - in a `panda`s dataframe `var_global_yearly_mean_anomaly`. We use this dataframe object because it features the plot function `hvplot` which we would like to use. We start by creating this dataframe based on the datasets which we got from intake."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lxr = list(xr_dset_dict.keys())\n",
    "columns = [\".\".join(elem.split(\".\")[1:4]) for elem in lxr]\n",
    "print(columns)\n",
    "var_global_yearly_mean_anomaly = pd.DataFrame(index=range(1850, 2101), columns=columns)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. For all datasets in our dictionary, we calculate the anomaly by substracting the the global mean of the reference period from the global yearly mean.\n",
    "3. We add the results to the dataframe. Only years that are in the dataset can be filled into the dataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for key in xr_dset_dict.keys():\n",
    "    print([\".\".join(key.split(\".\")[1:4])])\n",
    "    datatoappend = global_yearly_mean(xr_dset_dict[key])[0, :] - var_refmean\n",
    "    years = list(xr_dset_dict[key].get(variable_id).groupby(\"time.year\").groups.keys())\n",
    "    var_global_yearly_mean_anomaly.loc[\n",
    "        years, \".\".join(key.split(\".\")[1:4])\n",
    "    ] = datatoappend"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting the multimodel comparison of the global annual mean anomaly"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot = var_global_yearly_mean_anomaly.hvplot.line(\n",
    "    xlabel=\"Year\",\n",
    "    ylabel=label,\n",
    "    value_label=label,\n",
    "    legend=\"top_left\",\n",
    "    title=\"Global and yearly mean anomaly in comparison to 1851-1880\",\n",
    "    grid=True,\n",
    "    height=600,\n",
    "    width=820,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#hvplot.save(plot, \"globalmean-yearlymean-tas.html\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Used data\n",
    "\n",
    "- https://doi.org/10.22033/ESGF/CMIP6.6594\n",
    "- https://doi.org/10.22033/ESGF/CMIP6.2450\n",
    "- https://doi.org/10.22033/ESGF/CMIP6.1869\n",
    "- https://doi.org/10.22033/ESGF/CMIP6.2686\n",
    "- https://doi.org/10.22033/ESGF/CMIP6.2800\n",
    "- https://doi.org/10.22033/ESGF/CMIP6.2817"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We acknowledge the CMIP community for providing the climate model data, retained and globally distributed in the framework of the ESGF. The CMIP data of this study were replicated and made available for this study by the DKRZ.”"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
