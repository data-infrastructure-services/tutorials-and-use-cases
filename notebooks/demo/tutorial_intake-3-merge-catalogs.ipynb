{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Intake III - work with two catalogs and merge them"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "```{admonition} Overview\n",
    ":class: dropdown\n",
    "\n",
    "![Level](https://img.shields.io/badge/Level-Intermediate-orange.svg)\n",
    "\n",
    "\n",
    "🎯 **objectives**: Learn how to integrate `intake-esm` in your workflow\n",
    "\n",
    "⌛ **time_estimation**: \"60min\"\n",
    "\n",
    "☑️ **requirements**:\n",
    "\n",
    "- 20GB memory\n",
    "- `intake_esm.__version__ == 2023.4.*`, at least 10GB memory.\n",
    "- [pandas](https://pandas.pydata.org/)\n",
    "- [xarray](http://xarray.pydata.org/en/stable/)\n",
    "\n",
    "© **contributors**: k204210\n",
    "\n",
    "⚖ **license**:\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Agenda\n",
    ":class: tip\n",
    "\n",
    "Based on DKRZ's CMIP6 and CORDEX catalogs and a Pangeo's CMIP6 catalog, you learn in this part,\n",
    "\n",
    "> how to **merge catalogs** by combining their data bases with *differently formatted assets*.\n",
    "\n",
    "This includes\n",
    "\n",
    "1. [Loading catalogs with a user-defined set of attributes](#load)\n",
    "1. [Comparing meta data for checking for compatibility](#compare)\n",
    "1. Merging the data bases via [merge](#merge) or [concat](#concat)\n",
    "1. [Configure a catalog description for accessing datasets across projects](#across)\n",
    "1. [Make ALL data accessible and consolidate aggregation](#access)\n",
    "1. [Save the new catalog](#save)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Questions\n",
    ":class: questions \n",
    "\n",
    "- how can you find out how **compatible** the catalogs are? Would you have to sanitize the column names?\n",
    "- what is overlap? Which of the 1000 datasets of pange are included in DKRZ's?\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial highlights two use cases:\n",
    "\n",
    "1. Merging two projects (CMIP6 and CORDEX, both from DKRZ)\n",
    "1. Merging two data bases for the same project (CMIP6 DKRZ and CMIP6 Pangeo)\n",
    "\n",
    "For each, the ultimate **Goal** of this tutorial is to create a merged catalog which also enables data access to data sets of both catalogs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Case 1: Merge two projects CMIP6 and CORDEX in one catalog\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import intake\n",
    "import pandas as pd\n",
    "#dkrz_catalog=intake.open_catalog([\"https://dkrz.de/s/intake\"])\n",
    "#only for generating the web page we need to take the original link:\n",
    "dkrz_catalog=intake.open_catalog([\"https://gitlab.dkrz.de/data-infrastructure-services/intake-esm/-/raw/master/esm-collections/cloud-access/dkrz_catalog.yaml\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print([entry for entry in list(dkrz_catalog) if \"disk\" in entry and (\"cordex\" in entry or \"cmip6\" in entry)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"load\"></a>\n",
    "\n",
    "### Load catalogs with default + common columns\n",
    "\n",
    "Most of all DKRZ catalogs include [cataloonies](https://tutorials.dkrz.de/tutorial_intake-1-2-dkrz-catalogs.html) attributes. This simplifies the merging as you could already merge the catalogs over these columns. Usable columns of the catalogs are stored in the main catalog's metadata and can be displayed and retrieved:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dkrz_catalog.metadata"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "overall_columns=dkrz_catalog.metadata[\"parameters\"][\"cataloonie_columns\"][\"default\"]\n",
    "print(overall_columns)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, these attributes are not sufficient for finding an individual assets in CORDEX and CMIP6. We need *additional* columns:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cordex_columns=dkrz_catalog._entries[\"dkrz_cordex_disk\"]._open_args[\"read_csv_kwargs\"][\"usecols\"]\n",
    "print(cordex_columns)\n",
    "cmip6_columns=dkrz_catalog._entries[\"dkrz_cmip6_disk\"]._open_args[\"read_csv_kwargs\"][\"usecols\"]\n",
    "print(cmip6_columns)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We open both catalogs with the columns that we have found:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cmip6_cat=dkrz_catalog.dkrz_cmip6_disk(read_csv_kwargs=dict(usecols=cmip6_columns+overall_columns))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cordex_cat=dkrz_catalog.dkrz_cordex_disk(read_csv_kwargs=dict(usecols=cordex_columns+overall_columns))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We assume that we are interested in the variable **tas** which is the *Near-Surface Temperature*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cmip6_cat=cmip6_cat.search(variable_id=\"tas\")\n",
    "cordex_cat=cordex_cat.search(variable_id=\"tas\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"merge\"></a>\n",
    "\n",
    "### Merge both catalogs\n",
    "\n",
    "The underlying *DataFrames* have different columns. We add CMIP6 columns to the CORDEX catalog and vice versa so that we can merge:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for cordex_col in list(set(cordex_columns)-set(overall_columns)):\n",
    "    cmip6_cat.df.loc[:,cordex_col]=\"None\"\n",
    "for cmip6_col in list(set(cmip6_columns)-set(overall_columns)):\n",
    "    cordex_cat.df.loc[:,cmip6_col]=\"None\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for column in overall_columns+cmip6_columns+cordex_columns :\n",
    "    cmip6_cat.df[column]=cmip6_cat.df[column].astype(str)\n",
    "    cordex_cat.df[column]=cordex_cat.df[column].astype(str)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "overall_df=pd.merge(cmip6_cat.df, cordex_cat.df, on=overall_columns+cmip6_columns+cordex_columns, how=\"outer\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "overall_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"across\"></a>\n",
    "\n",
    "### Redefine catalog description\n",
    "\n",
    "We copy the entire `.json` description file so that we can edit it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mixed_esmcol_data=dict(cmip6_cat.esmcat).copy()\n",
    "mixed_esmcol_data[\"aggregation_control\"]=dict(mixed_esmcol_data[\"aggregation_control\"]).copy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mixed_esmcol_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a look at these entries. We can subdivide these into two groups:\n",
    "\n",
    "1. Required to be manually changed:\n",
    "- **groupby_attrs**: We will change it such that both CMIP6 and CORDEX datasets can be created.\n",
    "- **attributes** and **default_columns**: The CORDEX ones need to be added\n",
    "- **aggregation_control**: Must be revised but we can do that afterwards. For now, we will just delete all entries but the one for `variable_id`\n",
    "\n",
    "2. Other attributes\n",
    "- **assets**: Will stay the same as there is no difference between the original catalogs\n",
    "- **catalog_file**: Will be automatically overwritten by Intake when the final catalog is written.\n",
    "- **Description**, **esmcat_version**, **id**: Is arbitrary\n",
    "\n",
    "We will start with adding missing **attributes**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "columns_already=[dict(k)[\"column_name\"] for k in mixed_esmcol_data[\"attributes\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "columns_already"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for k in dict(cordex_cat.esmcat)[\"attributes\"] :\n",
    "    if dict(k)[\"column_name\"] not in columns_already:\n",
    "        mixed_esmcol_data[\"attributes\"].append(k)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**groupby_attrs**:\n",
    "\n",
    "he attributes used to build an index for a dataset is defined by the order of attributes in the list **groupby_attrs**. The aggregation methods for CMIP6 datasets and CORDEX datasets *differ*.\n",
    "\n",
    "We have to redefine this list. Think about the perfect order and arrangement of attributes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mixed_esmcol_data[\"aggregation_control\"][\"groupby_attrs\"]=[\n",
    "    \"CORDEX_domain\",\n",
    "    \"driving_model_id\",\n",
    "    \"activity_id\",\n",
    "    \"institute_id\",\n",
    "    \"model_id\",\n",
    "    \"experiment_id\",\n",
    "    \"frequency\",\n",
    "    \"table_id\",\n",
    "    \"grid_label\"\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**aggregation_control**\n",
    "\n",
    "For now, drop all the aggregation attributes besides `variable_id` for enabling a quick save of the catalog. Note that the grouping only works if there is at least one entry in the `mixed_esmcol_data[\"aggregation_control\"][\"aggregations\"]` list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for entry in mixed_esmcol_data[\"aggregation_control\"][\"aggregations\"]:\n",
    "    if dict(entry)[\"attribute_name\"] != \"variable_id\" :\n",
    "        mixed_esmcol_data[\"aggregation_control\"][\"aggregations\"].remove(entry)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mixed_esmcol_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`NaN`s cause trouble in the df, so that we set it to *notset*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for k in mixed_esmcol_data[\"aggregation_control\"][\"groupby_attrs\"]:\n",
    "    overall_df[overall_df[k].isna()]=\"notset\"\n",
    "    overall_df[k]=overall_df[k].str.replace(\"None\",\"notset\")\n",
    "    overall_df[k]=overall_df[k].str.replace(\"nan\",\"notset\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us open the combined intake catalog:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "cmip6andcordex=intake.open_esm_datastore(\n",
    "    obj=dict(\n",
    "        esmcat=mixed_esmcol_data,\n",
    "        df=overall_df\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We write the new catalog to disk via:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cmip6andcordex.serialize(\"test\", catalog_type=\"file\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can test if our configuration works by directly opening it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "intake.open_esm_datastore(\"test.json\").search(experiment_id=\"historical\",\n",
    "                                              source_id=\"MPI*\",\n",
    "                                              simulation_id=\"r1i1*\")#.to_dataset_dict(cdf_kwargs=dict(chunks=dict(time=1)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Case 2: Merge two data bases for CMIP6\n",
    "\n",
    "Assume you are interested in variable `tas` from table `Amon` from both catalogs.\n",
    "\n",
    "You would start look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pangeo=intake.open_catalog(\"https://raw.githubusercontent.com/pangeo-data/pangeo-datastore/master/intake-catalogs/master.yaml\")\n",
    "#\n",
    "print(list(pangeo.climate))\n",
    "cmip6_pangeo=intake.open_esm_datastore(\"https://storage.googleapis.com/cmip6/pangeo-cmip6.json\")\n",
    "cmip6_cat=dkrz_catalog.dkrz_cmip6_disk\n",
    "#\n",
    "esm_dkrz_tas=cmip6_cat.search(\n",
    "        variable_id=\"tas\",\n",
    "        table_id=\"Amon\"\n",
    ")\n",
    "esm_pangeo_tas=cmip6_pangeo.search(\n",
    "        variable_id=\"tas\",\n",
    "        table_id=\"Amon\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(esm_dkrz_tas)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(esm_pangeo_tas)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"compare\"></a>\n",
    "\n",
    "Let's\n",
    "\n",
    "### Compare the Metadata\n",
    "\n",
    "1. Both catalogs follow the [esmcat-specs](https://github.com/NCAR/esm-collection-spec/blob/master/collection-spec/collection-spec.md) which can be seen from the following entry:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(dict(esm_dkrz_tas.esmcat)[\"esmcat_version\"])\n",
    "print(dict(esm_pangeo_tas.esmcat)[\"esmcat_version\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. As both catalogs follow the esmcat standard, they have a list of `attributes` which we can compare: Indeed, they have exactly the same attributes/columns. In the following, we use pandas DataFrames for better displays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "esm_dkrz_atts_df=pd.DataFrame([dict(k) for k in dict(esm_dkrz_tas.esmcat)[\"attributes\"]])\n",
    "esm_pangeo_atts_df=pd.DataFrame([dict(k) for k in dict(esm_pangeo_tas.esmcat)[\"attributes\"]])\n",
    "esm_dkrz_atts_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "esm_pangeo_atts_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "esm_pangeo_atts_df.equals(esm_dkrz_atts_df)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When working with both catalogs, you would notice that the pangeo's do not use a prefix character 'v' for the values of *version* however dkrz does. We fix that with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "esm_pangeo_tas.df[\"version\"]= \"v\" + esm_pangeo_tas.df[\"version\"].astype(str)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. The data format: The pangeo catalog contains zarr datasets stored in the google cloud storage while dkrz's catalog allows different formats by providing a column named *format*. When we combine these catalogs, we have to consider the different formats"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "esm_pangeo_tas.esmcat.assets.format"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\n",
    "    esm_dkrz_tas.df[\"format\"].unique(),\n",
    "    esm_dkrz_tas.esmcat.assets.format_column_name\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"concat\"></a>\n",
    "\n",
    "### Combine the databases with the underlying DataFrames\n",
    "\n",
    "This is a workflow for creating a merged data base:\n",
    "\n",
    "1. Find all common column names/keys that are in both data bases.\n",
    "1. Create a filtered Catalog\n",
    "    1. Setting common columns as index in both catalogs\n",
    "    1. Throw out indices in one catalog that are in both.\n",
    "1. Concat the filtered catalog with the reference catalog.\n",
    "\n",
    "Let us start with 1.: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "keys = [key \n",
    "        for key in esm_dkrz_tas.df.columns.values \n",
    "        if key in esm_pangeo_tas.df.columns.values\n",
    "       ]\n",
    "keys"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We continue with 2.:\n",
    "\n",
    "> 2. Create a filtered Catalog\n",
    "\n",
    "We create a multi-index with all common keys with `set_index` and save these in new variables `i1` and `i2`. These can be used as a filter. The `~` sign reverses the condition in the filter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "i1 = esm_pangeo_tas.df.set_index(keys).index\n",
    "i2 = esm_dkrz_tas.df.set_index(keys).index\n",
    "esm_pangeo_tas_filtered=esm_pangeo_tas.df[~i1.isin(i2)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And finally, 3.\n",
    "\n",
    "> 3. Concat the filtered catalog with the reference catalog.\n",
    "\n",
    "We use pandas `concat` function and *ignore the indices* of both catalogs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "esm_merged=pd.concat([esm_dkrz_tas.df, esm_pangeo_tas_filtered],\n",
    "                     ignore_index=True)\n",
    "esm_merged"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"access\"></a>\n",
    "\n",
    "### Make ALL data accessible and consolidate aggregation\n",
    "\n",
    "Intake enables to load assets of different formats. For that, \n",
    "\n",
    "1. the data base must have a column which describes the **format** of the asset. \n",
    "1. only one column contains the information how to **access** the asset needs to be merged. In our example, the `zstore` column and the `uri` column needs to be merged into one common column. We name that `uri`.\n",
    "1. the **assets** entry in the catalog description needs to be adapted to the new configuration.\n",
    "\n",
    "We start with \n",
    "\n",
    "1. creating a 'format' column which is *zarr* if there is no entry in *uri* and *netcdf* in all other cases."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "esm_merged[\"format\"]=\"netcdf\"\n",
    "esm_merged.loc[pd.isna(esm_merged[\"uri\"]),\"format\"]=\"zarr\"\n",
    "esm_merged.loc[pd.isna(esm_merged[\"time_range\"]),\"time_range\"]=\"*\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. We merge the *zstore* and *uri* columns in a new column *uri*. As we need individual values of the asset, we have to loop over the rows.\n",
    "\n",
    "```{note}\n",
    "Whenever you can, you should omit using `iterrows` because it is rather slow.\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "esm_merged.loc[pd.isna(esm_merged[\"uri\"]),\"uri\"]=esm_merged[\"zstore\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "del esm_merged[\"zstore\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. We now create a new description.\n",
    "\n",
    "This will be based on the dkrz catalog. We use that because it has the aggregation over time which we want to maintain. \n",
    "\n",
    "The *assets* entry now does not have a direct description of the **format** but instead a specification of a **format_column_name**. Also, the *column_name* is *uri* instead of *path*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_cat_json=dict(esm_dkrz_tas.esmcat).copy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_cat_json[\"assets\"]={\n",
    "    \"column_name\":\"uri\",\n",
    "    \"format_column_name\":\"format\"\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_cat_json[\"id\"]=\"Merged dkrz-pangeo cmip6 subset catalog\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to make *zarr stores* compatible with the aggregation over time, we have to fill in a dummy value in *time_range*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "esm_merged.loc[pd.isna(esm_merged[\"time_range\"]),\"time_range\"]=\"*\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"save\"></a>\n",
    "\n",
    "### Save the new catalog\n",
    "\n",
    "Let us test the new catalog first. We can open the new catalog by providing two arguments to `open_esm_datastore`:\n",
    "\n",
    "- the data base **esm_merged**\n",
    "- the catalog description **new_cat_json**\n",
    "\n",
    "Afterwards, we search for a subset which is in both "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "esm_merged_cat=intake.open_esm_datastore(\n",
    "    dict(\n",
    "        esmcat=new_cat_json,\n",
    "        df=esm_merged\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "esm_merged_cat_test=esm_merged_cat.search(activity_id=\"ScenarioMIP\",\n",
    "                                          member_id=\"r1i1p1f1\",\n",
    "                                          grid_label=\"gn\",\n",
    "                                         source_id=[\"MPI-ESM1-2-HR\",\"CAS-ESM2-0\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since we have two different formats in the catalog, we have to provide keyword arguments for both formats within the `to_dataset_dict` function.\n",
    "\n",
    "- `zarr_kwargs={\"consolidated\":True}` is needed because Pangeo's zarr assets have *consolidated* metadata\n",
    "- `cdf_kwargs={\"chunks\":{\"time\":1}}` configures dask to not use very large arrays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_dsets=esm_merged_cat_test.to_dataset_dict(\n",
    "    zarr_kwargs={\"consolidated\":True},\n",
    "    cdf_kwargs={\"chunks\":{\"time\":1}}\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That worked fine. Now we save the catalog with `serialize`. We will separate the catalog into two files, the database `.csv.gz` file and the descriptor `.json` file. We can do that by passing the `catalog_type` keyword argument:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "esm_merged_cat.serialize(name=\"our_catalog\", catalog_type=\"file\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{seealso}\n",
    "This tutorial is part of a series on `intake`:\n",
    "* [Part 1: Introduction](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-1-introduction.html)\n",
    "* [Part 2: Modifying and subsetting catalogs](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-2-subset-catalogs.html)\n",
    "* [Part 3: Merging catalogs](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-3-merge-catalogs.html)\n",
    "* [Part 4: Use preprocessing and create derived variables](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-4-preprocessing-derived-variables.html)\n",
    "* [Part 5: How to create an intake catalog](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-5-create-esm-collection.html)\n",
    "\n",
    "- You can also do another [CMIP6 tutorial](https://intake-esm.readthedocs.io/en/latest/user-guide/cmip6-tutorial.html) from the official intake page.\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (based on the module python3/2023.01)",
   "language": "python",
   "name": "python3_2023_01"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
