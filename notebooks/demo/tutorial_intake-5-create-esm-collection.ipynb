{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "output_scroll"
    ]
   },
   "source": [
    "# Intake V - create intake-esm catalog from scratch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "output_scroll"
    ]
   },
   "source": [
    "```{admonition} Overview\n",
    ":class: dropdown\n",
    "\n",
    "![Level](https://img.shields.io/badge/Level-Intermediate-orange.svg)\n",
    "\n",
    "\n",
    "🎯 **objectives**: Learn how to create `intake-esm` ESM-collections\n",
    "\n",
    "⌛ **time_estimation**: \"60min\"\n",
    "\n",
    "☑️ **requirements**: `intake_esm.__version__ == 2023.4.*`, at least 10GB memory.\n",
    "- intake I, intake II\n",
    "\n",
    "- [pandas](https://pandas.pydata.org/)\n",
    "- [json](https://de.wikipedia.org/wiki/JavaScript_Object_Notation)\n",
    "- [xarray](http://xarray.pydata.org/en/stable/)\n",
    "\n",
    "© **contributors**: k204210\n",
    "\n",
    "⚖ **license**:\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "```{admonition} Agenda\n",
    ":class: tip\n",
    "\n",
    "In this part, you learn\n",
    "\n",
    "1. [When to build `intake-esm` collections](#motivation)\n",
    "1. [How to create a standardized intake-esm catalog from scratch](#create) \n",
    "    1. [How to equip the catalog with attributes and configurations for assets and aggregation](#description)\n",
    "    2. [How to add the collection of assets to the catalog](#database)\n",
    "1. [How to validate and save the newly created catalog](#validate)\n",
    "1. [How to configure the catalog to process multivariable assets](#multi)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Intake** is a cataloging tool for data repositories. It opens catalogs with *driver*s. Drivers can be plug-ins like `intake-esm`. \n",
    " \n",
    "This tutorial gives insight into the creation of a **intake-esm catalogs**. We recommend this specific driver for intake when working with ESM-data as the plugin allows to load the data with the widely used and accepted tool `xarray`. \n",
    "\n",
    "```{note}\n",
    "This tutorial creates a catalog from scratch. If you work based on another catalog, it might be sufficient for you to look into [intake II - save subset]() \n",
    "``` "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"motivation\"></a>\n",
    "\n",
    "## 1. When should I create an `intake-esm` catalog?\n",
    "\n",
    "Cataloging your data set with a *static* catalog for *easy access* is beneficial if \n",
    "- the data set is *stable* 🏔 such that you do not have to update the content of the catalog to make it usable at all\n",
    "- the data set is *very large* 🗃 such that browsing and accessing data via file system is less performant\n",
    "- the data set should be *shared* 🔀 with many people such that you cannot use a data base format"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "<a class=\"anchor\" id=\"create\"></a>\n",
    "\n",
    "## 2. Create an intake-esm catalog which complies to [esmcat-specs]((https://github.com/NCAR/esm-collection-spec/blob/master/collection-spec/collection-spec.md))\n",
    "\n",
    "In order to create a well-defined, helpful catalog, you have to answer the following questions:\n",
    "\n",
    "- What should be *search facetts* of the catalog?\n",
    "- How are [assets](#asset) of the catalog combined to a dataset?\n",
    "- How should `xarray` open the data set?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "For `intake-esm` catalogs, an early [standard](https://github.com/NCAR/esm-collection-spec/blob/master/collection-spec/collection-spec.md) has been developped to ensure compatibility across different `intake-esm` catalogs. We will follow those specs in this tutorial.\n",
    "\n",
    "In the code example, we will use a python dictionary in this example but you could also write directly into a file with your favorite editor. We start with a catalog dictionary `intake_esm_catalog` and add the required basic meta data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "intake_esm_catalog={\n",
    "    # we follow the esmcat specs version 0.1.0: \n",
    "    'esmcat_version': '0.1.0',\n",
    "    'id': 'Intake-esmI',\n",
    "    'description': \"This is an intake catalog created for the intake tutorial\"\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "<a class=\"anchor\" id=\"description\"></a>\n",
    "\n",
    "### 2.1. Create the description\n",
    "\n",
    "The description contains all the meta data which is necessary to understand the catalog. That makes the catalog *self-descriptive*. It also includes configuration for intake how to load assets of the data set(s) with the specified driver."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"defineatts\"></a>\n",
    "\n",
    "#### Define **attributes** of your catalog\n",
    "\n",
    "The catalog's [collection](#collection) uses attributes to describe the assets. These attributes are defined in the description via python `dict`ionaries and given as a list in the `intake-esm` catalog `.json` file, e.g.:\n",
    "\n",
    "```json\n",
    "\"attributes\": [\n",
    "    {\n",
    "      \"column_name\": \"activity_id\",\n",
    "      \"vocabulary\": \"https://raw.githubusercontent.com/WCRP-CMIP/CMIP6_CVs/master/CMIP6_activity_id.json\"\n",
    "    },\n",
    "]\n",
    "```\n",
    "\n",
    "and will be accessed by users from the loaded catalog variable `catalog` via:\n",
    "\n",
    "```python\n",
    "catalog.esmcat.attributes\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Catalog's attributes should allow users to \n",
    "\n",
    "- effectively **browse**: \n",
    "    - The in-memory representation and the visulation tool for the catalog is a `Pandas` *DataFrame*. By specifying a `column_name`, the columns of the *DataFrame* are generated by the attributes of the catalog.\n",
    "    - Additionally, the `column_name` of the catalog's attributes can be used as *search facetts* - they will be keyword arguments of the `catalog.search()` function\n",
    "- **understand** the content: You can provide information to the attributes, e.g. by specifying a `vocabulary` for all available (or allowed) values of the attribute.\n",
    "\n",
    "➡ The [collection](#collection) must have values for all defined attributes (see 3.2.)\n",
    "\n",
    "➡ In other terms: If [assets](#assets) should be integrated into the catalog, they have to be described with these attributes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "```{admonition} Best Practise\n",
    ":class: tip\n",
    "\n",
    "- The best configuration is reached if all datasets can be *uniquely identified*. I.e., if the users fill out all search facets, they will end up with **only one** dataset.\n",
    "- Do not exaggerate with supply of additional columns. Users may be confused when many search fields have similar meanings. Also, the display of the DataFrame should fit into the window width.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Use case: Catalog for project data on a file system**\n",
    "\n",
    "Given a more than one level directory tree, ensure that:\n",
    "\n",
    "- All files are on the same and deepest directory level.\n",
    "- Each directory level has the same meaning across the project data. E.g. the deepest directory can have the meaning **version**.\n",
    "\n",
    "This can easily be done by creating a **directory structure template** and check against their definitions.\n",
    "\n",
    "If that is approved, each directory level can be used as an catalog's attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "attributes=[]\n",
    "directory_structure_template=\"mip_era/activity_id/institution_id/source_id/experiment_id/member_id/table_id/variable_id/grid_label/version\"\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for att in directory_structure_template.split('/'):\n",
    "    attributes.append(\n",
    "        dict(column_name=att,\n",
    "             vocabulary=f\"https://raw.githubusercontent.com/WCRP-CMIP/CMIP6_CVs/master/CMIP6_{att}.json\"\n",
    "            )\n",
    "    )\n",
    "intake_esm_catalog[\"attributes\"]=attributes    \n",
    "print(intake_esm_catalog)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{note}\n",
    "For data managemant purposes in general, we highly recoomend to define a <i>path_template</i> and a <i>filename_template</i> for a clear directory structure **before storing any data**.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* You can add more attributes from files or by parsing filenames"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Define the **assets** column of your catalog\n",
    "\n",
    "The `assets` entry is a python `dict`ionary in the catalog similiar to an attribute, e.g.:\n",
    "\n",
    "```json\n",
    "  \"assets\": {\n",
    "    \"column_name\": \"path\",\n",
    "    \"format\": \"netcdf\"\n",
    "  },\n",
    "```\n",
    "\n",
    "The assets of a catalog refer to the data source that can be loaded by `intake`. Assets are essential for connecting `intake`'s function of **browsing** with the function of **accessing** the data. It contains \n",
    "\n",
    "- a `column_name` which is associated with the keyword in the [collection](#collection). The value of `column_name` in the collection points at the [asset](#asset) which can be loaded by `xarray`.\n",
    "- the entry `format` specifies the dataformat of the asset.\n",
    "\n",
    "```{note}\n",
    "\n",
    "If you have [assets](#asset) of mixed types, you can substitute `format` by `format_column_name` so that both information for the asset is taken from the [collection](#collection)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "assets={\n",
    "    \"column_name\": \"path\",\n",
    "    \"format\": \"netcdf\"\n",
    "  }\n",
    "intake_esm_catalog[\"assets\"]=assets\n",
    "print(intake_esm_catalog)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Optional: Define **aggregation control** for your data sets\n",
    "\n",
    "```{note}\n",
    "\n",
    "If **aggregation_control** is not defined, intake opens one xarray dataset per asset\n",
    "\n",
    "```\n",
    "\n",
    "One goal of a catalog is to the make access of the data as **analysis ready** as possible. Therefore, `intake-esm` features aggregating multiple [assets](#asset) to a larger single **data set**. If **aggregation_control** is defined in the [catalog](#catalog) and users run the catalog's `to_dataset_dict()` function, a Python dictionary of aggregated xarray datasets is created. The logic for merging and/or concatenating the catalog into datasets has to be configured under aggregation_control.\n",
    "\n",
    "The implementation works such that the variable's dimensions are either enhanced by a new dimension or an existing dimension is extended with new data included in the addtional [assets](#asset)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- `aggregation_control` is a `dict`ionary in the [catalog](#catalog). If it is set, three keywords have to be configured:\n",
    "    - `variable_column_name`: In the [collection](#collection), the **variable name** is specified under that column. Intake-esm will aggregate [assets](#asset) with the same name only.  Thus, all [assets](#asset) to be combined to a dataset have to include at least one unique variable. If your [assets](#asset) contain more than one data variable and users should be able to subset with intake, check [multi variable assets](#multivar).\n",
    "    - `groupby_attrs`: [assets](#asset) attributed with different values of the `groupby_attrs` **should not be aggregated** to one xarray dataset. E.g., if you have data for different ESMs in one catalog you do not want users to merge them into one dataset. The `groupby_attrs` will be combined to the key of the aggregated dataset in the returned dictionary of `to_dataset_dict()`.\n",
    "    - `aggregations`: Specification of **how** xarray should combine [assets](#asset) with same values of these `groupby_attrs`. <a class=\"anchor\" id=\"aggregations\"></a>\n",
    "\n",
    ". E.g.:\n",
    "```json\n",
    "  \"aggregation_control\": {\n",
    "    \"variable_column_name\": \"variable_id\",\n",
    "    \"groupby_attrs\": [\n",
    "      \"activity_id\",\n",
    "      \"institution_id\"\n",
    "    ],\n",
    "    \"aggregations\": [\n",
    "      {\n",
    "        \"type\": \"union\",\n",
    "        \"attribute_name\": \"variable_id\"\n",
    "      }\n",
    "    ]\n",
    "  }\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start with defining `variable_column_name` and `groupby_attrs`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aggregation_control=dict(\n",
    "    variable_column_name=\"variable_id\",\n",
    "    groupby_attrs=[\n",
    "        \"activity_id\",\n",
    "        \"institution_id\"\n",
    "    ]\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "```{admonition} Best Practise\n",
    ":class: tip\n",
    "\n",
    "- A well-defined aggregation control contains **all** [defined attributes](#defineatts)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[**Aggregations**](#aggregations):\n",
    "\n",
    "*aggregations* is an optional list of dictionaries each of which configures\n",
    "\n",
    "- on which dimension of the variable the [assets](#assets) should be aggregated\n",
    "- optionally: what keyword arguments should be passed to xarray's `concat()` and `merge()` functions\n",
    "\n",
    "for one attribute/column of the catalog given as `attribute_name`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A dictionary of the aggregations list is named *aggregation object* and has to include three specifications:\n",
    "- `attribute_name`: the column name which is not a *groupby_attr* and should be used for aggregating a single variable over a dimension\n",
    "- `type`: Can either be\n",
    "    - `join_new`: \n",
    "    - join_existing\n",
    "    - union\n",
    "- **optional**: `options`: Keyword arguments for xarray"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following defines that `variable_id` will be taken for a unique dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aggregation_control[\"aggregations\"]=[dict(\n",
    "    attribute_name=\"variable_id\",\n",
    "    type=\"union\"\n",
    ")]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we configure intake to use `time` for extending the existing dimension `time`. Therefore, we have to add `options` with \"dim\":\"time\" as keyword argument for xarray:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aggregation_control[\"aggregations\"].append(\n",
    "    dict(\n",
    "        attribute_name=\"time_range\",\n",
    "        type=\"join_existing\",\n",
    "        options={ \"dim\": \"time\", \"coords\": \"minimal\", \"compat\": \"override\" }\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also, kind of retrospectively, combine all *member* of an ensemble on a new dimension of a variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aggregation_control[\"aggregations\"].append(\n",
    "    dict(\n",
    "        attribute_name= \"member_id\",\n",
    "        type= \"join_new\",\n",
    "        options={ \"coords\": \"minimal\", \"compat\": \"override\" }\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```{note}\n",
    "\n",
    "It is not possible to pre-configure `dask` options for `xarray`. Be sure that users of your catalog know if and how to set <b>chunks</b>.\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "intake_esm_catalog[\"aggregation_control\"]=aggregation_control\n",
    "print(intake_esm_catalog)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "<a class=\"anchor\" id=\"database\"></a>\n",
    "\n",
    "### 2.2. Create the data base for the catalog\n",
    "\n",
    "The [collection](#collection) of [assets](#asset) can be specified either\n",
    "- under `catalog_dict` as a list of dictionaries inside the [catalog](#catalog). One asset including all attribute specifications is saved as an individual dictionary, e.g.:\n",
    "```json    \n",
    "    \"catalog_dict\": [\n",
    "        {\n",
    "            \"filename\": \"/work/mh0287/m221078/prj/switch/icon-oes/experiments/khwX155/outdata/khwX155_atm_mon_18500101.nc\",\n",
    "            \"variable\": \"tas_gmean\"\n",
    "        }\n",
    "    ]\n",
    "```\n",
    "- or under `catalog_file` which refers to a separate `.csv` file, e.g.\n",
    "```json    \n",
    "    \"catalog_file\": \"dkrz_cmip6_disk_netcdf.csv.gz\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "### Option A: Catalog_dict implementation\n",
    "\n",
    "Assuming, we would like to create a catalog for all files in `/work/ik1017/CMIP6/data/CMIP6/ScenarioMIP/DKRZ/MPI-ESM1-2-HR/ssp370/r1i1p1f1/Amon/tas/gn/v20190710/`, we can parse the path with our `directory_structure_template`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trunk=\"/work/ik1017/CMIP6/data/CMIP6/ScenarioMIP/DKRZ/MPI-ESM1-2-HR/ssp370/r1i1p1f1/Amon/tas/gn/v20190710/\"\n",
    "trunkdict={}\n",
    "for i,item in enumerate(directory_structure_template.split('/')):\n",
    "    trunkdict[item]=trunk.split('/')[-(len(directory_structure_template.split('/'))-i+1)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "Afterwards, we can associate all files in that directory with these attributes and the additional `time_range` and `path` using os:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "filelist=!ls {trunk}\n",
    "catalog_dict=[]\n",
    "for asset in filelist:\n",
    "    assetdict={}\n",
    "    assetdict[\"time_range\"]=asset.split('.')[0].split('_')[-1]\n",
    "    assetdict[\"path\"]=trunk+asset\n",
    "    assetdict.update(trunkdict)\n",
    "    catalog_dict.append(assetdict)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "Then, we put that dict into the catalog:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "intake_esm_catalog[\"catalog_dict\"]=catalog_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "### Option B: Catalog_file implementation\n",
    "\n",
    "The `catalog_file` format needs to comply with the following rules:\n",
    "\n",
    "- all file types that can be opened by pandas are allowed to be set as `catalog_file`\n",
    "- the `.csv` file needs a header which includes all catalog attributes\n",
    "\n",
    "An example would be:\n",
    "```csv\n",
    "filename,variable\n",
    "/work/mh0287/m221078/prj/switch/icon-oes/experiments/khwX155/outdata/khwX155_atm_mon_18500101.nc,tas_gmean\n",
    "``` \n",
    "\n",
    "```{note}\n",
    "\n",
    "- Note that the *catalog_file* can also live in the cloud i.e. be an URL. You can host both the collection and catalog in the cloud as [DKRZ](https://swiftbrowser.dkrz.de/public/dkrz_a44962e3ba914c309a7421573a6949a6/intake-esm/ ) does.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "```{admonition} Best practice\n",
    ":class: tip\n",
    "\n",
    "For keeping clear overview, you better use the same prefix name for both `catalog` and `catalog_file`.\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "catalog_dict_df=pd.DataFrame(catalog_dict)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Saving a separate data base for [assets](#asset) or use a dictionary in the catalog?\n",
    "\n",
    "```{tabbed} Advantages catalog_dict\n",
    "\n",
    "- Only maintain one file which contains both catalog and collection\n",
    "\n",
    "Suitable for smaller catalogs\n",
    "\n",
    "```\n",
    "\n",
    "```{tabbed} Disadvantages catalog_dict\n",
    "\n",
    "- you cannot easily compress the catalog\n",
    "- you can only use **one type of data access** for the catalog content. For CMIP6, we can provide access via `netcdf` or via `opendap`. We can create two collections for the same catalog file for covering both use cases.\n",
    "\n",
    "Suitable for larger catalogs\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Use case: Updating the collection for a living project on file system\n",
    "\n",
    "Solution: Write a **builder** script and run it as a cronjob (automatically and regularly):\n",
    "\n",
    "A typical builder for a community project contains the following sequence:\n",
    "\n",
    "1. Create one or more **lists of files** based on a `find` shell command on the data base directory. This type of job is also named *crawler* as it *crawls* through the file system.\n",
    "1. Read the lists of files and create a `panda`s DataFrame for these files.\n",
    "1. Parse the file names and file paths and fill column values. That can be easily done by deconstructing filepaths and filenames into their parts assuming you defined a mandatory\n",
    "    - Filenames that cannot be parsed should be sorted out\n",
    "1. The data frame is saved as the final **catalog** as a `.csv` file. You can also compress it to `.csv.gz`.\n",
    "    \n",
    "At DKRZ, we run scripts for project data on disk repeatedly in cronjobs to keep the catalog updated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Builder tool examples\n",
    "\n",
    "- The NCAR [builder tool](https://github.com/NCAR/intake-esm-datastore/tree/e253f184ccc78906a08f1580282da070b898957a/builders) for community projects like CMIP6 and CMIP5.\n",
    "- DKRZ builder notebooks (based on NCAR tools) like this [Era5 notebook](https://gitlab.dkrz.de/data-infrastructure-services/intake-esm/-/blob/master/builder/notebooks/dkrz_era5_disk_catalog.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<a class=\"anchor\" id=\"validate\"></a>\n",
    "\n",
    "## 3. Validate and save the catalog:\n",
    "\n",
    "If we open the defined catalog with `open_esm_datastore()` and try `to_dataset_dict()`, we can check if our creation is successful. The resulting catalog should give us exactly 1 dataset from 18 assets as we aggregate over time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import intake\n",
    "validated_cat=intake.open_esm_datastore(\n",
    "    obj=dict(\n",
    "        df=catalog_dict_df,\n",
    "        esmcat=intake_esm_catalog\n",
    "    )\n",
    ")\n",
    "validated_cat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "validated_cat.to_dataset_dict()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Intake esm allows to write catalog file(s) with the `serialize()` function. The only argument is the **name** of the catalog which will be used as filename. It writes the two parts of the catalog either together in a `.json` file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "validated_cat.serialize(\"validated_cat\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or in two seperated files if we provide `catalog_type=file` as a second argument. The `test.json` may be very large while we can save disk space if we svae the data base in a separate `.csv.gz` file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "validated_cat.serialize(\"validated_cat\", catalog_type=\"file\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<a class=\"anchor\" id=\"multi\"></a>\n",
    "\n",
    "## 4. Multivariable assets\n",
    "\n",
    "If an [asset](#asset) contains more than one variable, `intake-esm` also features pre-selection of a variable before loading the data. [Here](https://intake-esm.readthedocs.io/en/latest/user-guide/multi-variable-assets.html) is a user guide on how to configure the collection for that.\n",
    "\n",
    "1. the *variable_column* of the catalog must contain iterables (`list`, `tuple`, `set`) of values.\n",
    "2. the user must specifiy a dictionary of functions for converting values in certain columns into iterables. This is done via the `csv_kwargs` argument such that the collection needs to be opened as follows:\n",
    "\n",
    "```python\n",
    "import ast\n",
    "import intake\n",
    "\n",
    "col = intake.open_esm_datastore(\n",
    "    \"multi-variable-collection.json\",\n",
    "    csv_kwargs={\"converters\": {\"variable\": ast.literal_eval}},\n",
    ")\n",
    "col\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{seealso}\n",
    "This tutorial is part of a series on `intake`:\n",
    "* [Part 1: Introduction](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-1-introduction.html)\n",
    "* [Part 2: Modifying and subsetting catalogs](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-2-subset-catalogs.html)\n",
    "* [Part 3: Merging catalogs](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-3-merge-catalogs.html)\n",
    "* [Part 4: Use preprocessing and create derived variables](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-4-preprocessing-derived-variables.html)\n",
    "* [Part 5: How to create an intake catalog](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-5-create-esm-collection.html)\n",
    "\n",
    "- You can also do another [CMIP6 tutorial](https://intake-esm.readthedocs.io/en/latest/user-guide/cmip6-tutorial.html) from the official intake page.\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (based on the module python3/2023.01)",
   "language": "python",
   "name": "python3_2023_01"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
