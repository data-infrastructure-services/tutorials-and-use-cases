{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## The DKRZ CMIP Data Pool\n",
    "\n",
    "This demo is about the content of the data pool at dkrz mainly on the example of **CMIP6**. We show you\n",
    "- an introduction into CMIP6\n",
    "- how to use `intake-esm` and `pandas` to investigate the pool\n",
    "- that analysis services profit from an elaborate CMIP data infrastructure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    " > The [DKRZ CMIP data pool](https://www.dkrz.de/up/services/data-management/cmip-data-pool) contains often needed flagship collections of climate model data, is hosted as part of the DKRZ data infrastructure and supports scientists in high volume climate data collection, access and processing. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Highlights of these climate model data collections:\n",
    " - [CMIP6](https://www.wcrp-climate.org/wgcm-cmip/wgcm-cmip6): The recent phase of the Coupled Model Intercomparison Project\n",
    " - [CMIP5](https://www.wcrp-climate.org/wgcm-cmip/wgcm-cmip5)\n",
    " - [CORDEX](https://cordex.org/): The Coordinated Regional Downscaling Experiment.\n",
    " - [ERA5](https://www.dkrz.de/up/services/data-management/projects-and-cooperations/era): Weather data from the [European Centre for Medium-Range Weather Forecasts](http://www.ecmwf.int) by re-analysed and homogenised observation data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Understanding CMIP6 data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### The goal of CMIP6:\n",
    "\n",
    "In order to evaluate and compare climate models, a globally organized intercomparison project is periodically conducted.\n",
    "CMIP6 tackles three major questions:\n",
    "\n",
    "- How does the Earth system respond to forcing?\n",
    "- What are the origins and consequences of systematic model biases?\n",
    "- How can we assess future climate changes given internal climate variability, predictability, and uncertainties and scenarios?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<div>\n",
    "<img src=\"https://mpimet.mpg.de/fileadmin/projekte/CMIP6/160621_CMIP6_Organigramm_web.png\" width=\"400\"/>\n",
    "</div>\n",
    "\n",
    "From [Eyring et al., 2016](https://gmd.copernicus.org/articles/9/1937/2016/). Schematic of the CMIP/CMIP6 experiment design\n",
    "\n",
    "The CMIP6 framework allows smaller model intercomparison projects (MIPs) with a specific focus to be endorsed to CMIP6. That means, each model that runs the standard CMIP experiments can participate in CMIP6 and further MIPs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Required Attributes and Controlled Vocabularies\n",
    "\n",
    "Besides the technical requirements, the CMIP data standard defines **required attributes** in so called [**Controlled Vocabularies (CV)**](https://github.com/WCRP-CMIP/CMIP6_CVs). While some values are predefined, models and institutions have to be registered to become a valid value of corresponding attributes. For many attributes, both a short form with `_id` and a longer description exist."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Important required attributes:\n",
    "\n",
    "- `activity_id`: A CMIP6-endorsed MIP that investigates a specific research question. It defines `experiment`s and requests data for it.\n",
    "- `source_id`  : An ID for the Earth System Model used to produce the data.\n",
    "- `member_id`  : The ensemble member. All members should be statistically equal. \n",
    "- `grid_label` : Specifies if the raw model output grid is used or if the data has been regridded to another. The native grid is reported with `grid_label=gn`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import intake\n",
    "col = intake.open_esm_datastore(\"/mnt/lustre02/work/ik1017/Catalogs/mistral-cmip6.json\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "unique_activites=col.unique(\"activity_id\")\n",
    "print(list(unique_activites[\"activity_id\"].values()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "unique_sources=col.unique(\"source_id\")\n",
    "print(\"Number of unique earth system models in the cmip6 data pool: \"+str(list(unique_activites[\"source_id\"].values())[0]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "unique_members=col.unique(\"member_id\")\n",
    "list(unique_members[\"member_id\"].values())[1][0:3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Data Reference Syntax\n",
    "\n",
    "An atomic **Dataset** contains all files which cover the entire time span of a single variable of a single simulation. This can be multiple files in one.\n",
    "\n",
    "The Data Reference Syntax (DRS) is a set of *required attributes* which **uniquely identify** and describe a dataset. The DRS usually includes all attributes used in the path templates so that both words are used synonymously. The DRS elements are arranged to a **hierarchical** path template for CMIP6:\n",
    "\n",
    "CMIP6: `mip_era`/`activity_id`/`institution_id`/`source_id`/`experiment_id`/`member_id`/`table_id`/`variable_id`/`grid_label`/`version`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "*Be careful when browsing through the CMIP6 data tree!*\n",
    "\n",
    "**Unique** in CMIP6 data hierarchy:\n",
    "- `experiment_id` in `activity_id` \n",
    "- `variable_id` in `table_id` : Both combined represent the **CMIP Variable**\n",
    "- Only one `version` for one dataset should be published"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# Searching for the MIP which defines the experiment 'historical':\n",
    "\n",
    "cat = col.search(experiment_id=\"historical\")\n",
    "cat.unique(\"activity_id\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# Searching for all tables which contain the variable 'tas':\n",
    "\n",
    "cat = col.search(variable_id=\"tas\")\n",
    "cat.unique(\"table_id\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "cat = col.search(variable_id=\"tas\", table_id=\"Amon\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Not **Unique** in CMIP6 data hierarchy:\n",
    "- `institution_id` for both `source_id` + `experiment_id` ( + `member_id` )\n",
    "\n",
    "No requirements for `member_id`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# Searching for all institution_ids which uses the model 'MPI-ESM1-2-HR' to produce 'ssp585' results:\n",
    "\n",
    "cat = col.search(source_id=\"MPI-ESM1-2-HR\", experiment_id=\"ssp585\")\n",
    "cat.unique(\"institution_id\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# Searching for all experiment_ids produced with ESM 'EC-Earth3' and as ensemble member 'r1i1p1f1':\n",
    "\n",
    "cat = col.search(source_id=\"EC-Earth3\", member_id=\"r1i1p1f1\")\n",
    "cat.unique(\"experiment_id\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# Searching for all valid ensemble member_ids produced with ESM 'EC-Earth3' for experiment 'abrupt-4xCO2'\n",
    "\n",
    "cat = col.search(source_id=\"EC-Earth3\", experiment_id=\"abrupt-4xCO2\")\n",
    "cat.unique(\"member_id\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "⇨ **Do not** search for `institution_id`, `table_id` and `member_id` unless you are sure about what you are doing.\n",
    "Instead, begin to search for\n",
    "`experiment_id`, `source_id`, `variable_id`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### How can I find the variables I need?\n",
    "\n",
    "1. **[Search](https://cfconventions.org/Data/cf-standard-names/77/build/cf-standard-name-table.html) for the matching `standard_name`**\n",
    "\n",
    "Most of the data in the data pool is compliant to the [Climate and Forecast Convention](https://cfconventions.org/). This defines, among others, so called `standard_names`, which need to be assigned to variables as a variable attribute inside the data. Since these are very long, the name of the variable in the data is a shorter - a so-called short name. This short name is saved in the data catalogs which can be searched."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "2. **[Search](http://clipc-services.ceda.ac.uk/dreq/mipVars.html) for corresponding `short_name`s in the CMIP6 data request**\n",
    "\n",
    "E.g., you get many results for `air_temperature`. Multiple definitions for one ‘physical’ variable like air_temperature exist in CMIP: Each is a combination of requirements for \n",
    "- frequency\n",
    "- time cell methods (average or instantaneous)\n",
    "- vertical level (e.g. interpolated on pressure levels)\n",
    "- grid\n",
    "- realm (e.g. atmosphere model output or ocean model output)\n",
    "of the variable.\n",
    "\n",
    "This requirements is set according to the interest of the MIPs. Variables with the same requiremnts are collected in one MIP-table which can be identified by `table_id`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Investigating the pool content"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import intake\n",
    "col = intake.open_esm_datastore(\"/mnt/lustre02/work/ik1017/Catalogs/mistral-cmip6.json\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def pieplot(gbyelem) :\n",
    "    size = col.df.groupby([gbyelem]).size().sort_values(ascending=False)\n",
    "    size10 = size.nlargest(10)\n",
    "    size10[9] = sum(size[9:])\n",
    "    size10.rename(index={size10.index.values[9]:'all other'},inplace=True)\n",
    "    return size10.plot.pie(figsize=(18,8),ylabel='',autopct='%.2f',)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pieplot(\"experiment_id\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pieplot(\"source_id\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Benefits from the CMIP data infrastructure\n",
    "\n",
    "The CMIP6 data at DKRZ comprises about 4 PB. In order to tackle the challenges of data provision and dissemination for such a repository size, a [state-of-the-art data infrastructure](https://www.dkrz.de/c6de/dicad/dicad?set_language=en&cl=en) has been developed around that pool. In the following, we highlight three aspects of the data workflow."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Data quality\n",
    "\n",
    "CMIP6 data is only available in a common and **reliable** [Data format](https://goo.gl/neswPr)\n",
    "- No adaptions needed for output of specific models\n",
    "- Makes data **interoperable** and enables general cmip-analysing software products as ESMVal\n",
    "\n",
    "CMIP6 data was **quality controlled** before published with [PrePARE](https://cmor.llnl.gov/mydoc_cmip6_validator/)\n",
    "- We can ensure e.g. correct coordinates and attribute configurations in the data\n",
    "\n",
    "CMIP6 data is **transparent** about occuring errors\n",
    "- Search the [errata](errata.es-doc.org/) data base for origins of suspicious analysis results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Data publication\n",
    "\n",
    "- **Find and access** data via the fail-safe [ESGF portal](esgf-data.dkrz.de) due to the redundant network of ESGF nodes\n",
    "- Exentended **documentation** for simulation conducts provided in the [ES-Doc](https://explore.es-doc.org/) data base\n",
    "- [**Persistent Identfier**](https://esgf-data.dkrz.de/projects/esgf-dkrz/pid) (PIDs) ensure long-term webaccess to dataset information\n",
    "- [**Citation information**](cmip6cite.wdc-climate.de) and DOIs for all published datasets easily retrievable"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Data curation\n",
    "\n",
    "The CMIP6 data pool is maintained by\n",
    "- automatic **ingest** and **egest** which keeps the data updated\n",
    "- **Back ups** of the data pool in the tape archive\n",
    "- **Long term archival** for a range of datasets used in the IPCC report"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "⇨  High quality Analysis services only because of a high quality data supply\n",
    "\n",
    "Keep *Acknowledging* and *requesting* high data quality!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "python3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}