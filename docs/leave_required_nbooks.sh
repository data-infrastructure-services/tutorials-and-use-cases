#!/bin/sh
if [ "${CI_COMMIT_BRANCH}" != "master" ]; then
  a=$(git diff --name-only $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA | grep ipynb | rev | cut -d '/' -f -1 | rev | tr '\n' ' ')
  for nb in $(ls source/*.ipynb); do
          sed -i "s;exclude_patterns:;exclude_patterns:\n    - ${nb};g" source/_config.yml
  done  
  for nb in $a;
  do
    nb2="source/"$nb""
    echo $nb2
    sed -i "s;    - ${nb2};;g" source/_config.yml
  done
fi
cd ../notebooks/
jupyter nbconvert --to python clear_and_prepare_for_ci.ipynb
ipython clear_and_prepare_for_ci.py
cd ../notebooks/


