.. _faq:

FAQs
====

Visit the `DKRZ website <https://www.dkrz.de/>`_ to know more about us. 


How to contact us
*****************
Please, write an email to data-pool@dkrz.de.

How to find info about the DKRZ Jupyterhub/lab
**********************************************
Please, visit this DKRZ website `<https://www.dkrz.de/up/systems/jupyterhub-dkrz.de-1/jupyterhub-dkrz.de>`_. 
Find  a video tutorial `here <https://youtu.be/f0wZX9i0uWQ>`_.