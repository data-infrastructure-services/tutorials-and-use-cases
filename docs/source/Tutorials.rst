.. Tutorials and Use Cases documentation master file, created by
   sphinx-quickstart on Tue Jan 19 15:58:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Tutorials
===================================================

This section contains a series of Notebooks for different python packages which are supported at DKRZ and which we recommend you to use. 
Each training series aims at making you fully capable of applying the python package for enhanced climate data analysis.

.. toctree::
   :maxdepth: 1

   Intake
   Quality
   tutorial_cloud_tzis.ipynb
