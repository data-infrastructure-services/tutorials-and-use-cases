.. Tutorials and Use Cases documentation master file, created by
   sphinx-quickstart on Tue Jan 19 15:58:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tutorials and Use Cases's!
===================================================

|Gitlab-repo| |Pages|

.. |Gitlab-repo| image:: https://img.shields.io/badge/gitlab-repo-green
   :target: https://gitlab.dkrz.de/data-infrastructure-services/tutorials-and-use-cases

.. |Pages| image:: https://img.shields.io/badge/gitlab-pages-blue
      :target: https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/index.html

This `Gitlab repository <https://gitlab.dkrz.de/data-infrastructure-services/tutorials-and-use-cases/>`_ collects and prepares Jupyter notebooks with coding examples on how to use state-of-the-art *processing tools* on *big data* collections. The Jupyter notebooks highlight the optimal usage of *High-Performance Computing resources* and adress data analysists and researchers which begin to work with resources of German Climate Computing Center `DKRZ <https://www.dkrz.de/>`_.

The Jupyter notebooks are meant to run in the `Jupyterhub portal <https://jupyterhub.dkrz.de/>`_. See in this `video <https://youtu.be/f0wZX9i0uWQ>`_ the main features of the DKRZ Jupterhub/lab and how to use it. Clone this repositroy into your home directory at the DKRZ supercomputers Levante and Mistral. The contents will be visible from the Jupyterhub portal. When you open a notebook in the Jupyterhub, make sure you choose a *recent* Python 3 kernel on the Kernel tab (upper tool bar in the Jupyterhub). Such a kernel contains most of the common geoscience packages in current versions.

Direct and fast access to DKRZ's data pools is a main benefit of the `server-side <https://en.wikipedia.org/wiki/Server-side>`_ data-near computing demonstrated here. Note that running the notebooks on your local computer will generally require much memory and processing resources.

Find a video tutorial `here <https://youtu.be/f0wZX9i0uWQ>`_.


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   README
   Tutorials
   Use-cases

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
