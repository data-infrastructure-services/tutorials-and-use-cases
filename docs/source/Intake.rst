Intake
===================================================

This is a training series on the cataloging package *intake*.

.. toctree::
   :maxdepth: 1

   tutorial_intake-1-introduction.ipynb
   tutorial_intake-1-2-dkrz-catalogs.ipynb
   tutorial_intake-1-3-dkrz-catalogs-era5.ipynb
   tutorial_intake-2-subset-catalogs.ipynb
   tutorial_intake-3-merge-catalogs.ipynb
   tutorial_intake-4-preprocessing-derived-vars.ipynb
   tutorial_intake-5-create-esm-collection.ipynb
