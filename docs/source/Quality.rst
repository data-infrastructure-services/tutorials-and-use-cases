.. Tutorials and Use Cases documentation master file, created by
   sphinx-quickstart on Tue Jan 19 15:58:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Data Quality and Compression
===================================================

This series is on different data and meta data quality checker which are used for ensuring high data quality when publishing to data repositories like ESGF.

.. toctree::
   :maxdepth: 1

   tutorial_data-quality_atmodat-checker.ipynb
   tutorial_cdocmor.ipynb
   tutorial_compression_netcdf.ipynb
