.. _user_guide:

User Guide
==========

Information for model data users
********************************

In the folder "notebooks" in the Tutorial and Use Cases `Gitlab repository <https://gitlab.dkrz.de/data-infrastructure-services/tutorials-and-use-cases/>`_ you can find Jupyter notebooks with coding examples showing how to use Big Data and High-Performance Computing software for applications in geoscience. 

The Jupyter notebooks are meant to run in the Jupyterhub server of the German Climate Computing Center `DKRZ <https://www.dkrz.de/>`_ which is an `ESGF <https://esgf.llnl.gov/>`_ repository that hosts 4 petabytes of `CMIP6 <https://pcmdi.llnl.gov/CMIP6/>`_ model data (more info on the data pool in this `doc <https://www.dkrz.de/up/services/data-management/cmip-data-pool>`_)). 

See in this `video <https://youtu.be/f0wZX9i0uWQ>`_ the main features of the DKRZ Jupterhub/lab and how to use it. Clone this tutorial-and-use-cases repo in your home at the DKRZ supercomputer "Mistral" or just download from here and upload there the notebook of your interest. Then, the repo or just the notebook will be visible from the Jupyterhub. When you open a notebook in the Jupyterhub, please, choose the Python 3 unstable kernel on the Kernel tab (upper tool bar in the Jupyterhub). This kernel contains all the common geoscience packages. 

Besides the information on the Jupyterhub, in these DKRZ `docs <https://www.dkrz.de/up/systems/mistral/programming/jupyter-notebook>`_) you can find how to run Jupyter notebooks directly in the DKRZ server, that is, out of the Jupyterhub. It will entail that you create the environment accounting for the required package dependencies. 

Do not try to run these notebooks in your premise, which is also known as `client-side <https://en.wikipedia.org/wiki/Client-side>`_ computing. It will also require that you install the necessary packages on you own but it will anyway fail because you will not have direct access to the data pool. Direct access to the data pool is one of the main benefits of the `server-side <https://en.wikipedia.org/wiki/Server-side>`_ data-near computing demonstrated in these tutorials and use cases. 

Please, visit this `DKRZ website <https://www.dkrz.de/up/systems/jupyterhub-dkrz.de-1/jupyterhub-dkrz.de>`_. 

Find  a video tutorial `here <https://youtu.be/f0wZX9i0uWQ>`_.