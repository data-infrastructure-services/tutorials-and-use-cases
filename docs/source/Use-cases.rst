.. Tutorials and Use Cases documentation master file, created by
   sphinx-quickstart on Tue Jan 19 15:58:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Use-cases
===================================================

.. toctree::
   :maxdepth: 1

   use-case_advanced_summer_days_intake_xarray_cmip6.ipynb
   use-case_calculate-frost-days_intake-xarray_cmip6.ipynb
   use-case_multimodel-comparison_xarray-cdo_cmip6.ipynb
   use-case_plot-unstructured_psyplot_cmip6.ipynb
   use-case_simple-vis_xarray-matplotlib_cmip6.ipynb
   use-case_global-yearly-mean-anomaly_xarray-hvplot_cmip6.ipynb
   use-case_convert-nc-to-tiff_rioxarray-xesmf_cmip.ipynb
   use-case_climate-extremes-indices_cdo.ipynb
   use-case_ensemble-analysis_intake-xarray_cmip6.ipynb
   app-intake.ipynb
