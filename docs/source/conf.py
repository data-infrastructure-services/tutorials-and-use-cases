# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Tutorials and Use Cases'
copyright = '2021, Data Infra team'
author = 'Data Infra team'

# The full version, including alpha/beta/rc tags
release = '0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
templates_path = ['_templates']
with open('_templates/imprint.html', 'r') as f :
    imprint=f.read()

extensions = [
#  "nbsphinx",
#  "myst_parser",
  "sphinx_panels",
  "sphinx_copybutton",
  "myst_nb"
]
panels_add_bootstrap_css = False

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

#nbsphinx_allow_errors = True
nb_execution_allow_errors = True
#nbsphinx_execute = 'never'
#jupyter_execute_notebooks = "off"
#jupyter_execute_notebooks = "force"
nbsphinx_kernel_name = 'python3'
#nbsphinx_timeout = -1
nb_execution_timeout = -1
execution_excludepatterns = ['*era5*.ipynb']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', '**.ipynb_checkpoints']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_book_theme'
html_theme_options = {
    #'show_prev_next': False,
    'repository_url': 'https://gitlab.dkrz.de/data-infrastructure-services/tutorials-and-use-cases', #https://github.com/spatialaudio/nbsphinx',
    'use_repository_button': True,
    'use_edit_page_button': False,
    'use_fullscreen_button' : True,
    'use_download_button' : True,
    'extra_footer': imprint,
    "external_links": [
      {"name": "DKRZ Website", "url": "https://www.dkrz.de/"},
  ]
}


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
